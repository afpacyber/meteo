package fr.afpa.meteo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fr.afpa.meteo.models.forecast5.ForecastList;
import fr.afpa.meteo.utils.Constant;

public class ForecastAdapter extends ArrayAdapter<ForecastList> {

    private int resId;

    public ForecastAdapter(Context context, int resource, List<ForecastList> objects) {
        super(context, resource, objects);

        resId = resource; // R.layout.item_forecast
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // chargement du layout
        convertView = LayoutInflater.from(getContext()).inflate(resId, null);

        // récupération des Views
        TextView textViewDate = convertView.findViewById(R.id.textViewDate);
        TextView textViewTemperature = convertView.findViewById(R.id.textViewTemperature);
        ImageView imageViewIcon = convertView.findViewById(R.id.imageViewIcon);

        // récupération de l'objet ForecastList
        ForecastList item = getItem(position);

        // affichage de la date et de la température
        textViewDate.setText(item.getDt_txt());
        textViewTemperature.setText(item.getMain().getTemp());

        // affichage de l'image
        String urlImage = String.format(Constant.URL_IMAGE, item.getWeather().get(0).getIcon());
        Picasso.get().load(urlImage).into(imageViewIcon);

        return convertView;
    }
}
