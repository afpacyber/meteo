package fr.afpa.meteo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import fr.afpa.meteo.models.current.OpenWeatherMap;
import fr.afpa.meteo.models.forecast5.Forecast;
import fr.afpa.meteo.utils.Constant;
import fr.afpa.meteo.utils.FastDialog;
import fr.afpa.meteo.utils.Network;

public class MainActivity extends AppCompatActivity {
    private EditText editTextCity;
    private Button buttonSubmit;
    private TextView textViewCity;
    private TextView textViewTemperature;
    private ImageView imageViewIcon;
    private ListView listViewForecast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextCity = (EditText) findViewById(R.id.editTextCity);
        buttonSubmit = (Button) findViewById(R.id.buttonSubmit);
        textViewCity = (TextView) findViewById(R.id.textViewCity);
        textViewTemperature = (TextView) findViewById(R.id.textViewTemperature);
        imageViewIcon = (ImageView) findViewById(R.id.imageViewIcon);
        listViewForecast = (ListView) findViewById(R.id.listViewForecast);

        // submit button
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!editTextCity.getText().toString().isEmpty()) {

                    if (Network.isNetworkAvailable(MainActivity.this)) {

                        getVolley(Constant.PARAM_CURRENT);

                    } else {
                        FastDialog.showDialog(
                                MainActivity.this,
                                FastDialog.SIMPLE_DIALOG,
                                "Vous devez être connecté"
                        );
                    }

                } else { // no city
                    FastDialog.showDialog(
                            MainActivity.this,
                            FastDialog.SIMPLE_DIALOG,
                            "Vous devez renseigner une ville"
                    );
                }

            }
        });

    }

    private void getVolley(final String param) {
        // requête HTTP avec Volley
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        String url = String.format(Constant.URL_METEO, param, editTextCity.getText().toString());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String json) {

                        Log.e("json", "resultat: " + json);

                        if (param.equals(Constant.PARAM_CURRENT)) {
                            getDataCurrent(json);
                        } else if (param.equals(Constant.PARAM_FORECAST)) {
                            getDataForecast(json);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String json = new String(error.networkResponse.data);

                getDataCurrent(json);
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void getDataCurrent(String json) {
        // parsing du flux JSON
        Gson myGson = new Gson();
        OpenWeatherMap result = myGson.fromJson(json, OpenWeatherMap.class);

        if (result.getCod().equals("200")) {
            getVolley(Constant.PARAM_FORECAST);

            textViewCity.setText(result.getName());
            textViewTemperature.setText(result.getMain().getTemp());

            String urlImage = String.format(Constant.URL_IMAGE, result.getWeather().get(0).getIcon());
            Picasso.get().load(urlImage).into(imageViewIcon);
        } else {
            FastDialog.showDialog(MainActivity.this, FastDialog.SIMPLE_DIALOG, result.getMessage());
        }
    }

    private void getDataForecast(String json) {
        // parsing du flux JSON
        Gson myGson = new Gson();
        Forecast result = myGson.fromJson(json, Forecast.class);

        if (result.getCod().equals("200")) {
            Log.e("result", result.getList().get(0).getMain().getTemp());

            // TODO : affichage des Items dans la listViewForecast
            listViewForecast.setAdapter(
                    new ForecastAdapter(
                            MainActivity.this,
                            R.layout.item_forecast,
                            result.getList()
                    )
            );
        } else {
            FastDialog.showDialog(MainActivity.this, FastDialog.SIMPLE_DIALOG, result.getMessage());
        }
    }
}

