package fr.afpa.meteo.utils;

public class Constant {
    public static final String URL_METEO = "https://api.openweathermap.org/data/2.5/%s?q=%s,fr&units=metric&appid=a749c68b1d6b747844f278d71e4c718d";
    public static final String URL_IMAGE = "https://openweathermap.org/img/w/%s.png";

    public static final String PARAM_CURRENT = "weather";
    public static final String PARAM_FORECAST = "forecast";
}
