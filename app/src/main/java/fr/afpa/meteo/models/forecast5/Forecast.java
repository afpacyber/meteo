package fr.afpa.meteo.models.forecast5;

import java.util.List;

public class Forecast {
    //private String cod, message;
    private String cod;
    private String message;
    private List<ForecastList> list;

    public String getCod() {
        return cod;
    }

    public String getMessage() {
        return message;
    }

    public List<ForecastList> getList() {
        return list;
    }
}
