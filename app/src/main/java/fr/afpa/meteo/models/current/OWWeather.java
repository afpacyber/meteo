package fr.afpa.meteo.models.current;

public class OWWeather {
    private String icon;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
