package fr.afpa.meteo.models.forecast5;

import java.util.List;

public class ForecastList {
    private String dt_txt;
    private Main main;
    private List<Weather> weather;

    public class Main {
        private String temp;

        public String getTemp() {
            return temp;
        }

        @Override
        public String toString() {
            return temp;
        }
    }

    public class Weather {
        private String icon;

        public String getIcon() {
            return icon;
        }
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public Main getMain() {
        return main;
    }

    public List<Weather> getWeather() {
        return weather;
    }
}
