package fr.afpa.meteo.models.current;

public class OWMain {
    private String temp;

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
}
